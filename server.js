const express = require("express");
const unirest = require("unirest");
const request = require("request");
const axios = require("axios");
const http = require("https");

const app = express();

function unirest() {
  var req = unirest(
    "GET",
    "https://rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com/services/api/IchibaTag/Search/20140222"
  );

  req.query({
    tagId: "1000317",
  });

  req.headers({
    "x-rapidapi-host":
      "rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com",
    "x-rapidapi-key": "6aecc52055mshf918d80759bd343p133b39jsne6576e8108e2",
    useQueryString: true,
  });

  req.end(function (res) {
    if (res.error) throw new Error(res.error);

    console.log(res.body);
  });
}

function request() {
  var options = {
    method: "GET",
    url:
      "https://rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com/services/api/IchibaTag/Search/20140222",
    qs: { tagId: "1000317" },
    headers: {
      "x-rapidapi-host":
        "rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com",
      "x-rapidapi-key": "6aecc52055mshf918d80759bd343p133b39jsne6576e8108e2",
      useQueryString: true,
    },
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
  });
}

function http() {
  var options = {
    method: "GET",
    hostname:
      "rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com",
    port: null,
    path: "/services/api/IchibaTag/Search/20140222?tagId=1000317",
    headers: {
      "x-rapidapi-host":
        "rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com",
      "x-rapidapi-key": "6aecc52055mshf918d80759bd343p133b39jsne6576e8108e2",
      useQueryString: true,
    },
  };

  var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
      chunks.push(chunk);
    });

    res.on("end", function () {
      var body = Buffer.concat(chunks);
      console.log(body.toString());
    });
  });

  req.end();
}

function axios() {
  axios({
    method: "GET",
    url:
      "https://rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com/services/api/IchibaTag/Search/20140222",
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host":
        "rakuten_webservice-rakuten-marketplace-tag-search-v1.p.rapidapi.com",
      "x-rapidapi-key": "6aecc52055mshf918d80759bd343p133b39jsne6576e8108e2",
      useQueryString: true,
    },
    params: {
      tagId: "1000317",
    },
  })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    });
}

// unirest();
// request();
// http();
// axios();

app.listen(3333);
